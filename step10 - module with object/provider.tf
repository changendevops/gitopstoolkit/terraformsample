terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "3.15.0"
    }
  }
}

provider "vault" {
  address = var.vault_url
  token = var.vault_token
}