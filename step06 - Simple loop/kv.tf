resource "vault_mount" "this" {
  for_each = toset( var.kv_path )
  path        = each.key
  type        = "kv"
  options     = { version = "2" }
  description = "kv for ${each.key}"
}