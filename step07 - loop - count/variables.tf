variable "vault_url" {
  type        = string
  description = "Url of Vault"
}

variable "vault_token" {
  type        = string
  description = "Token to connect to vault"
}

variable "kv_path" {
  type        = string
  description = "Path of the kv"
}