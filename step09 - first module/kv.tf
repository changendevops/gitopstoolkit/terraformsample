module "kv2_denis" {
  source          = "./modules/kv"
  name            = var.kv.name
  description     = var.kv.description
}