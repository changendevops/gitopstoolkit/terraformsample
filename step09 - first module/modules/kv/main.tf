resource "vault_mount" "this" {
  path        = "${var.name}"
  type        = "kv"
  options     = { version = "2" }
  description = "${var.description}"
}