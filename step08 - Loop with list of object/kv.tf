resource "vault_mount" "this" {
  count          = length(var.kvs)
  path        = "${element(var.kvs, count.index).name}"
  type        = "kv"
  options     = { version = "2" }
  description = "${element(var.kvs, count.index).description}"
}