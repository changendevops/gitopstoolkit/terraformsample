resource "vault_mount" "this" {
  path        = var.kv_path
  type        = "kv"
  options     = { version = "2" }
  description = "kv for ${var.kv_path}"
}