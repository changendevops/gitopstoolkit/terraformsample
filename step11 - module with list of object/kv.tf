module "kv2" {
  count          = length(var.kvs)
  source = "./modules/kv"
  kv   = element(var.kvs, count.index)
}