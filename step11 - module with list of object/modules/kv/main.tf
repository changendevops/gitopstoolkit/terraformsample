resource "vault_mount" "this" {
  path        = "${var.kv.name}"
  type        = "kv"
  options     = { version = "2" }
  description = "${var.kv.description}"
}