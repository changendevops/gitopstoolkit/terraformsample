variable "kv" {
  type = object({
    name = string
    description = string
  })
}